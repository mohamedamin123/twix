import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { utilisateur } from '../models/utilisateur.model';

const baseUrl = 'http://localhost:8080/utilisateur';
@Injectable({
  providedIn: 'root'
})
export class UserCRUDService {


  constructor(private http: HttpClient) { }

  getAll(): Observable<utilisateur[]> {
    return this.http.get<utilisateur[]>(baseUrl+"/all");
  }

  get(id: any): Observable<utilisateur> {
    return this.http.get(`${baseUrl}/${id}`);
  }


  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  //deleteAll(): Observable<any> {
 //   return this.http.delete(baseUrl);
 // }

 // findByTitle(title: any): Observable<utilisateur[]> {
  //  return this.http.get<utilisateur[]>(`${baseUrl}?title=${title}`);
 // }
}
