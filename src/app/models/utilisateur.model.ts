export class utilisateur {
  id?: BigInteger;
  firstname?: string;
  lastname?: string;
  username?: string;
  email?: string;
  city?:string;
  phone?: string;
}
